#!/usr/bin/env bash
#
# Copyright 2021-2023 Chris Croome
#
# This file is part of the Webarchitects localhost Ansible repo.
#
# The Webarchitects localhost Ansible repo is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects localhost Ansible repo is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects localhost Ansible repo. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

md5sum_before="$(md5sum "${0}" | awk '{ print $1 }')"

git pull || exit

md5sum_after="$(md5sum "${0}" | awk '{ print $1 }')"

if [[ "${md5sum_after}" != "${md5sum_before}" ]]; then
  echo "The ${0} script has been updated, please run it again."
  exit
fi

hostname=$(hostname -s)

if [[ -f "${hostname}.yml" ]]; then
  ansible-galaxy install -r requirements.yml --force yarn || exit
  ansible-playbook "${hostname}.yml" --diff -t yarn
else
  ansible-galaxy install -r requirements.yml --force yarn || exit
  ansible-playbook localhost.yml --diff -t yarn
fi

