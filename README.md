# Ansible localhost Playbook

[![pipeline status](https://git.coop/webarch/localhost/badges/main/pipeline.svg)](https://git.coop/webarch/localhost/-/commits/main)

This repo contains Ansible Playbooks to install software on the `localhost` using Ansible Galaxy roles.

The usage instructions below assume no major edits are done to this repo locally apart from customising the roles to be run and how they are run, if you want to do things like specify versions of roles in the [req.yml](req.yml) playbook that is used to generate the [requirements.yml](requirements.yml) file or add additional roles then you should probably fork this repo.

## Usage

Clone this repo and then copy the [localhost.yml](localhost.yml) playbook:

```bash
git clone https://git.coop/webarch/localhost.git
cd localhost
cp localhost.yml $(hostname -s).yml
```

Then edit the `$(hostname).yml` file as required, for example if you don't need a prompt for the password for `sudo` or if you want to customise which roles are run, by default no roles are run.

Then run the [install.sh](install.sh) script:

```bash
./install.sh
```

The Bash scripts this repo includes:

### ansible.sh

The [ansible.sh](ansible.sh) script runs the [Ansible role](https://git.coop/webarch/ansible) as the user running the script. It doesn't require `root` or `sudo`.

### firefox.sh

The [firefox.sh](firefox.sh) script runs the [Firefox role](https://git.coop/webarch/firefox) using the [firefox.yml](firefox.yml) playbook which prompts for a password for `sudo`.

### install.sh

The [install.sh](install.sh) script installs all the requirements and runs the `$(hostname).yml` file if it exists and if not the [localhost.yml](localhost.yml) playbook.

### requirements.sh

The [requirements.sh](requirements.sh) script uses the [req.yml](req.yml) playbook to update and test the [requirements.yml](requirements.yml) file and the installs all the roles into `galaxy/roles`.

### update.sh

The [update.sh](update.sh) script runs the `$(hostname).yml` file if it exists and if not the [localhost.yml](localhost.yml) playbook.
ile.

## Copyright

Copyright 2021-2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
