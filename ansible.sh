#!/usr/bin/env bash
#
# Copyright 2021-2023 Chris Croome
#
# This file is part of the Webarchitects localhost Ansible repo.
#
# The Webarchitects localhost Ansible repo is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects localhost Ansible repo is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects localhost Ansible repo. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

export ANSIBLE_CONFIG="ansible.cfg"
export DEBIAN_FRONTEND="noninteractive"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

distro=$(grep '^ID=' /etc/os-release | awk -F "=" '{print $2}')
hostname=$(hostname -s)
version=$(grep '^VERSION_CODENAME=' /etc/os-release | awk -F "=" '{print $2}')

# Currently there is no jc in trixie
# https://packages.debian.org/trixie/jc
if [[ "${distro}" == "debian" ]]
then
if [[ "${version}" == "trixie" ]]
then
requirements="ansible-core
ansible-lint
git
jo
libpq-dev
pipx
postgresql-client
postgresql-common
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-mysqldb
python3-netaddr
python3-pip
python3-pymysql
python3-psycopg
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
# Bookworm ships python3-psycopg for psycopg3
elif [[ "${version}" == "bookworm" ]]
then
requirements="ansible
ansible-lint
git
jc
jo
libpq-dev
pipx
postgresql-client
postgresql-common
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-mysqldb
python3-netaddr
python3-pip
python3-pymysql
python3-psycopg
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
else
requirements="ansible
ansible-lint
git
jc
jo
libpq-dev
pipx
postgresql-client
postgresql-common
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-mysqldb
python3-netaddr
python3-pip
python3-pymysql
python3-psycopg2
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
fi
elif [[ "${distro}" == "ubuntu" ]]
then
requirements="ansible
ansible-lint
git
jc
jo
pipx
postgresql-client
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-mysqldb
python3-netaddr
python3-pip
python3-pymysql
python3-psycopg2
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
else
  echo "This script only supports debian and ubuntu"
  exit
fi

if [[ "$(id -u)" != "0" ]];
then
  for req in ${requirements}; do
    dpkg -s "${req}" &>/dev/null || (>&2 echo "Please run: sudo apt install ${req}" ; exit 1)
  done
elif [[ "$(id -u)" == "0" ]];
then
  # shellcheck disable=SC2086
  apt install -y ${requirements} || exit
fi

md5sum_before="$(md5sum "${0}" | awk '{ print $1 }')"

git pull || exit

md5sum_after="$(md5sum "${0}" | awk '{ print $1 }')"

if [[ "${md5sum_after}" != "${md5sum_before}" ]]; then
  echo "The ${0} script has been updated, please run it again."
  exit
fi

pipx_version=$(pipx --version)
# pipx environment was added in version 1.1.0
pipx_test_version=$(printf "${pipx_version}\n1.1.0" | sort --version-sort --reverse | tail -n1)
if [[ "${pipx_test_version}" != "1.1.0" ]]
then
  if [[ "$(id -u)" == "0" ]];
  then
    pipx_local_venvs="/opt/pipx/venvs"
  else
    pipx_local_venvs="${HOME}/.local/pipx/venvs"
  fi
else
  pipx_local_venvs=$(pipx environment | grep ^PIPX_LOCAL_VENVS | sed 's/PIPX_LOCAL_VENVS=//')
fi

# Debug packages and versions
which ansible-galaxy && \
  # Prior to ansible-galaxy version 2.13.1 there is no JSON output
  # https://git.coop/webarch/ansible/-/blob/cd2132c03dd5485e3a010458765fe3f7e2b3d21f/tasks/check_galaxy.yml#L70
  ansible-galaxy collection list
which ansible && \
  ansible --version
which jc && \
  # jc >= 1.18.8 supports --version
  # https://github.com/kellyjonbrazil/jc/releases/tag/v1.18.8
  # jammy has 1.17.3
  # https://packages.ubuntu.com/jammy/jc
  jc --version || jc -h

# Get the first digit of the installed version of community.general
comgen_installed=$(/usr/bin/ansible-galaxy collection list | grep community.general | sort -n | tail -n1 | awk '{ print $2 }' | sed 's/[.][0-9][.][0-9]$//')
if [[ "${comgen_installed}" -lt 7 ]];
then
  # Install community.general 7.5.9
  /usr/bin/ansible-galaxy collection install "community.general:==7.5.9" || exit
else
  echo "The version of community.general installed appears to be greater than 7.5.9, so skipping the comgen install"
fi

# Install the ansible roles
/usr/bin/ansible-galaxy install -r requirements.yml --force || exit

# Delete the ansible venv
if [[ "${1}" != "--check" && "${1}" != "-C" ]];
then
  if [[ -d "${pipx_local_venvs}/ansible" ]];
  then
    echo "In 10 seconds the existing Ansible venv at ${pipx_local_venvs}/ansible will be deleted, prior to reinstalling, hit Ctrl-C to prevent this!"
    sleep 10
    rm -rf "${pipx_local_venvs}/ansible"
  fi
else
  if [[ -d "${pipx_local_venvs}/ansible" ]];
  then
    echo "If this script was not running in check mode the pipx venv at ${pipx_local_venvs}/ansible would be deleted prior to running the ansible role"
  fi
fi

if [[ "${#}" -eq 0 ]]; then
  if [[ -f "ansible_${hostname}.yml" ]]; then
    echo "Running: ansible-playbook ansible_${hostname}.yml -t ansible --diff"
    /usr/bin/ansible-playbook "ansible_${hostname}.yml" -t ansible --diff
  else
    echo "Running: ansible-playbook ansible.yml --diff"
    /usr/bin/ansible-playbook ansible.yml --diff
  fi
elif [[ "${1}" == "--check" || "${1}" == "-C" ]];
then
  if [[ -f "ansible_${hostname}.yml" ]]; then
    echo: "Running ansible-playbook ansible_${hostname}.yml -t ansible --diff --check"
    /usr/bin/ansible-playbook "ansible_${hostname}.yml" -t ansible --diff --check
  else
    echo "Running: ansible-playbook ansible.yml --diff --check"
    /usr/bin/ansible-playbook ansible.yml --diff --check
  fi
elif [[ "${1}" == "--verbose" || "${1}" == "-v" ]];
then
  if [[ -f "ansible_${hostname}.yml" ]]; then
    echo "ansible-playbook ansible_${hostname}.yml -t ansible --diff -v"
    /usr/bin/ansible-playbook "ansible_${hostname}.yml" -t ansible --diff -v
  else
    echo "ansible-playbook ansible.yml --diff -v"
    /usr/bin/ansible-playbook ansible.yml --diff -v
  fi
else
  echo "This script only supports the --check / -C or --verbose / -v arguments."
  exit 1
fi

