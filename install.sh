#!/usr/bin/env bash
#
# Copyright 2021-2023 Chris Croome
#
# This file is part of the Webarchitects localhost Ansible repo.
#
# The Webarchitects localhost Ansible repo is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects localhost Ansible repo is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects localhost Ansible repo. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

hostname=$(hostname -s)

# Check if the script is being run by root
if [[ "$(id -u)" == "0" ]] ; then
  echo "The '$0' script has been designed to be run by a user who needs to enter a password to run sudo"
  exit 1
fi

if [[ -f "${hostname}.yml" ]]; then
  if [[ "${1}" != "" ]]; then
    ansible-playbook "${hostname}.yml" --diff -t "${1}"
  else
    ansible-playbook "${hostname}.yml" --diff
  fi
else
  if [[ "${1}" != "" ]]; then
    ansible-playbook localhost.yml --diff -t "${1}"
  else
    ansible-playbook localhost.yml --diff
  fi
fi
